from flask import render_template, flash, request, redirect, url_for
from app import app, models, db
from .forms import RegtistrationForm, LoginForm, PostsForm
from .models import Users
import datetime

@app.route('/', methods=['GET', 'POST'])
def index():
    # user = {'name': 'Stefan R'}
    # if request.method == 'POST':
    #   for posts in Posts.query.all():
    #     db.session.delete(posts),
    #     db.session.commit()
    #   flash('Wipe successful')
    return render_template('index.html',
                           Users = models.Users.query.all(),
                           title='Home')

@app.route('/Register', methods=['GET', 'POST'])
def Register():
    form = RegtistrationForm()
    if form.validate_on_submit():
        user = models.Users(FirstName=form.FName.data,
                           LastName=form.LName.data,
                           Email=form.Email.data,
                           Password=form.Password.data,
                           Date=datetime.datetime.utcnow()
                          )
        db.session.add(user)
        db.session.commit()
        flash('Succesfully received form data')
        return redirect(url_for('index'))
    return render_template('Register.html',
                           title='Register',
                           form=form)

@app.route('/Login', methods=['GET', 'POST'])
def Login():
    form = LoginForm()
    # if form.validate_on_submit():
    #     task = models.Task(Message1=form.text1.data,
    #                        Message2=form.text2.data)
    #     db.session.add(task)
    #     db.session.commit()
    #         return redirect(url_for('index'))
    return render_template('Login.html',
                           title='Login',
                           form=form)



@app.route('/Posts', methods=['GET', 'POST'])
def Posts():
        form = PostsForm()
        if form.validate_on_submit():
            p=models.Posts(UserPost=form.userPost.data, PostDate=datetime.datetime.utcnow())
            u = models.Users.query.filter_by(Email=form.Email.data).first()
            p.users = u
            db.session.add(p)
            db.session.commit()
            flash('Succesfully received form data')
        return render_template('Posts.html',
                          title='database view',
                          form = form
                          )

@app.route('/Users2', methods=['GET', 'POST'])
def database():
  # form = BlankForm()
  # if request.method == 'POST':
  #   theID = request.form
  #   updatetask = models.Task.query.get(theID)
  #   updatetask.Complete=1
  #   db.session.commit()
  return render_template('database.html',
                          title='database view',
                          Users = Users.query.all()
                          )
