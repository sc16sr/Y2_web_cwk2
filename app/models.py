from app import db
import datetime

class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Email = db.Column(db.String(500), unique=True, index=True)
    FirstName = db.Column(db.String(500))
    LastName = db.Column(db.String(500))
    Password = db.Column(db.String(500))
    Date = db.Column(db.DateTime)
    posts = db.relationship('Posts', backref='users', lazy='dynamic')

    def __repr__(self):
        return '' % (self.id, self.Email, self.FirstName, self.LastName, self.Password, self.Date)

class Posts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    UserPost = db.Column(db.String(500))
    PostDate = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __repr__(self):
        return '' % (self.id, self.UserPost)
