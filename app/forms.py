from flask_wtf import Form
from wtforms import TextField, PasswordField, IntegerField, BooleanField, TextAreaField
from wtforms.validators import DataRequired, Email, Length

class RegtistrationForm(Form):
    FName = TextField('FName', validators=[DataRequired()])
    LName = TextField('LName', validators=[DataRequired()])
    Email = TextField('Email', validators=[DataRequired(), Email()])
    Password = PasswordField('Password', validators=[DataRequired(), Length(min=8, max=32)])

class LoginForm(Form):
    Email = TextField('Email', validators=[DataRequired(), Email()])
    Password = PasswordField('Password', validators=[DataRequired(), Length(min=8, max=32)])

class PostsForm(Form):
    Email = TextField('Email', validators=[DataRequired(), Email()])
    userPost = TextField('Post', validators=[DataRequired(), Length(min=1, max=1000)])
